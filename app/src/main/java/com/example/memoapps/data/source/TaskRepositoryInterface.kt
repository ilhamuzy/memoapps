package com.example.memoapps.data.source

import com.example.memoapps.data.model.TaskEntity

interface TaskRepositoryInterface {

    fun insertTask(taskEntity: TaskEntity) : Long?
    fun getData(): List<TaskEntity>?
    fun updateTask(taskEntity: TaskEntity): Int?
    fun deleteData(taskEntity: TaskEntity): Int?
}