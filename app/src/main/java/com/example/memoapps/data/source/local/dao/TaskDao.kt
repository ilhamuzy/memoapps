package com.example.memoapps.data.source.local.dao

import androidx.room.*
import com.example.memoapps.data.model.TaskEntity

@Dao
interface TaskDao {

    @Insert
    fun insertTask(taskEntity: TaskEntity) : Long

    @Query("SELECT * FROM TaskEntity")
    fun getAll() : List<TaskEntity>

    @Update
    fun updateData(taskEntity: TaskEntity) : Int

    @Delete
    fun deleteData(taskEntity: TaskEntity) : Int
}