package com.example.memoapps.data.source

import android.content.Context
import com.example.memoapps.data.model.TaskEntity
import com.example.memoapps.data.source.local.DatabaseManager

class TaskRepository(context: Context) : TaskRepositoryInterface {

    private var mDatabaseManager : DatabaseManager? = null
    init {
        mDatabaseManager = DatabaseManager.getInstance(context)
    }

    override fun insertTask(taskEntity: TaskEntity): Long? {
        return mDatabaseManager?.taskDao()?.insertTask(taskEntity)
    }

    override fun getData(): List<TaskEntity>? {
        return mDatabaseManager?.taskDao()?.getAll()
    }

    override fun updateTask(taskEntity: TaskEntity): Int? {
        return mDatabaseManager?.taskDao()?.updateData(taskEntity)
    }

    override fun deleteData(taskEntity: TaskEntity): Int? {
        return mDatabaseManager?.taskDao()?.deleteData((taskEntity))
    }
}