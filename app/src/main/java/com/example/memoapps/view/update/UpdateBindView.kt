package com.example.memoapps.view.update

interface UpdateBindView {
    fun onSuccessUpdate(result: String)
}