package com.example.memoapps.view.main

import com.example.memoapps.data.source.TaskRepositoryInterface

class MainPresenterImplement
    (private val mainBindView: MainBindView,
    private val taskRepositoryInterface: TaskRepositoryInterface): MainPresenter{

    override fun getDataTask() {
        val result = taskRepositoryInterface.getData()
        mainBindView.onSuccess("Success Get Data", result)
    }
}