package com.example.memoapps.view.update

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.memoapps.R
import com.example.memoapps.data.model.TaskEntity
import com.example.memoapps.data.source.TaskRepository
import com.example.memoapps.data.source.TaskRepositoryInterface
import com.example.memoapps.view.main.MainActivity
import kotlinx.android.synthetic.main.activity_update.*

class UpdateActivity : AppCompatActivity(), UpdateBindView {

    lateinit var taskRepositoryInterface: TaskRepositoryInterface
    lateinit var updatePresenter: UpdatePresenter

    val data: TaskEntity? by lazy {
        intent.getParcelableExtra<TaskEntity>("data")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)

        taskRepositoryInterface = TaskRepository(this)
        updatePresenter = UpdatePresenterImplementation(this,taskRepositoryInterface)

        etUpdateTitle.setText(data?.title)
        etUpdateDescription.setText(data?.description)

        setSupportActionBar(tbUpdate)

        btnUpdate.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onSuccessUpdate(result: String) {
        etUpdateDescription.setText("")
        etUpdateTitle.setText("")
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater:MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.menuSave -> {
                val title = etUpdateTitle.text.toString()
                val description = etUpdateDescription.text.toString()
                updatePresenter.updateTask(TaskEntity(data?.id,title,description))
                true
            }else -> return super.onOptionsItemSelected(item)
        }
    }

}