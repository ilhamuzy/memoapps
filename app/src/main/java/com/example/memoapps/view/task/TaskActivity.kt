package com.example.memoapps.view.task

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.memoapps.R
import com.example.memoapps.data.model.TaskEntity
import com.example.memoapps.data.source.TaskRepository
import com.example.memoapps.data.source.TaskRepositoryInterface
import com.example.memoapps.view.main.MainActivity
import kotlinx.android.synthetic.main.activity_task.*

class TaskActivity : AppCompatActivity(), TaskView{

    lateinit var taskRepositoryInterface: TaskRepositoryInterface
    lateinit var taskPresenter: TaskPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        taskRepositoryInterface = TaskRepository(this)
        taskPresenter = TaskPresenterImplement(this, taskRepositoryInterface)
        setSupportActionBar(tbTask)

        btnTask.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onSuccessInsert(result: String) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        etTaskTitle.setText("")
        etTaskDescription.setText("")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.menuSave -> {
                val title = etTaskTitle.text.toString()
                val description = etTaskDescription.text.toString()
                taskPresenter.insertTask(TaskEntity(null,title,description))
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}