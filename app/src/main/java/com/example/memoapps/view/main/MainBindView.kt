package com.example.memoapps.view.main

import com.example.memoapps.data.model.TaskEntity

interface MainBindView {
    fun onSuccess(msg: String, result: List<TaskEntity>?)
}