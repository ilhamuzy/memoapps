package com.example.memoapps.view.update

import com.example.memoapps.data.model.TaskEntity
import com.example.memoapps.data.source.TaskRepositoryInterface

class UpdatePresenterImplementation
    (private val updateBindView: UpdateBindView,
     private val taskRepositoryInterface: TaskRepositoryInterface)
    : UpdatePresenter{

    override fun updateTask(taskEntity: TaskEntity) {
        val result = taskRepositoryInterface.updateTask(taskEntity)
        if (result !=0){
            updateBindView.onSuccessUpdate("Success Update Data")
        }else{
            updateBindView.onSuccessUpdate("Gagal Update Data")
        }
    }
}