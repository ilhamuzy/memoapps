package com.example.memoapps.view.update

import com.example.memoapps.data.model.TaskEntity

interface UpdatePresenter {
    fun updateTask(taskEntity: TaskEntity)
}