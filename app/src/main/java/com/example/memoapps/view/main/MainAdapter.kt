package com.example.memoapps.view.main

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.memoapps.R
import com.example.memoapps.data.model.TaskEntity
import com.example.memoapps.data.source.local.DatabaseManager
import com.example.memoapps.view.update.UpdateActivity
import kotlinx.android.synthetic.main.item_notes.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class MainAdapter(val listMemo: List<TaskEntity>?, var context: Context) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title = itemView.tvTitle
        var description = itemView.tvDescription
        var delete = itemView.ivDelete
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_notes, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = listMemo?.get(position)?.title.toString()
        holder.description.text = listMemo?.get(position)?.description.toString()

        holder.itemView.setOnClickListener {
            val intent = Intent(context, UpdateActivity::class.java)
            intent.putExtra("data", listMemo?.get(position))
            context.startActivity(intent)
        }
        holder.delete.setOnClickListener {
            AlertDialog.Builder(it.context).setPositiveButton("Yes"){
                p0, p1 ->
                val mdb = DatabaseManager.getInstance(holder.itemView.context)

                GlobalScope.async {
                    val result = listMemo?.get(position)?.let { it1 ->
                        mdb?.taskDao()?.deleteData(
                            it1
                        )
                    }

                    (holder.itemView.context as MainActivity).runOnUiThread {
                        if (result != 0){
                            Toast.makeText(
                                it.context,
                                "Data ${listMemo?.get(position)?.title} berhasil dihapus",
                                Toast.LENGTH_SHORT
                            ).show()
                        }else{
                            Toast.makeText(
                                it.context,
                                "Data${listMemo?.get(position)?.title} gagal dihapus",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    (holder.itemView.context as MainActivity).onSuccess("Success get data", listMemo)
                }
            }. setNegativeButton("Tidak"){
                p0, p1 ->
                p0.dismiss()
            }.setMessage("Apakah anda yakin menghapus data tersebut").setTitle("Konfirmasi Hapus").create().show()
        }
    }

    override fun getItemCount(): Int = listMemo?.size ?: 0

}