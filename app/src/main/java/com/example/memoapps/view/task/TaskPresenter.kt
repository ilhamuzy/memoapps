package com.example.memoapps.view.task

import com.example.memoapps.data.model.TaskEntity

interface TaskPresenter {
    fun insertTask(taskEntity: TaskEntity)
}