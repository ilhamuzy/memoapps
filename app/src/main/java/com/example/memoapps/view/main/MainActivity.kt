package com.example.memoapps.view.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.memoapps.R
import com.example.memoapps.data.model.TaskEntity
import com.example.memoapps.data.source.TaskRepository
import com.example.memoapps.data.source.TaskRepositoryInterface
import com.example.memoapps.view.task.TaskActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainBindView {

    lateinit var taskRepositoryInterface: TaskRepositoryInterface
    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        taskRepositoryInterface = TaskRepository(this)
        mainPresenter = MainPresenterImplement(this, taskRepositoryInterface)
        mainPresenter.getDataTask()

        btnAdd.setOnClickListener {
            startActivity(Intent(this, TaskActivity::class.java))
        }
    }

    override fun onSuccess(msg: String, result: List<TaskEntity>?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        rvRoom.apply {
            val adapter = MainAdapter(result, this@MainActivity)
            rvRoom.adapter = adapter
            setHasFixedSize(true)
        }
    }
}